(function(Drupal, $) {

  Drupal.behaviors.mobile_vertical_tables = {
    attach: function(context, settings) {

    var $verticalTable = $(".campl-vertical-stacking-table");

    //cycle through every vertical table on the page and insert table headers into table cells for mobile layout
    $verticalTable.each(function (i) {

        var $table = $(this);

        if($table.data('mobile-vertical-tables-processed')) {
          return true;
        }

        //for vertical stacking tables need to read the text value of each TH in turn and assign to the headers array
        var $tableHeaders = $(this).find('thead').find("th");

        var headerTextArray = [];
        //insert th value into every data set row in order
        //each loop to push into data array
        $tableHeaders.each(function (i) {
            headerTextArray.push($(this).text());
        });

        //for every row, insert into td append before text in td insert span to handle styling of header and data
        var $verticalTableRows = $(this).find("tr");

        $verticalTableRows.each(function (i) {
            //need to find all children of the table rows, (and not just table data cells)
            var $tableCells = $(this).children();
            $tableCells.each(function (i) {
                if(headerTextArray[i]) {
                    $(this).prepend("<span class='campl-table-heading'>"+headerTextArray[i]+"</span>")
                }
            })

        })

        $table.data('mobile-vertical-tables-processed', true);
    })

    }
  }

}) (Drupal, jQuery);
