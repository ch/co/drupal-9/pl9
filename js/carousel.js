(function(Drupal, $) {
  Drupal.behaviors.configure_carousel = {
    carousel_configured: false,
    attach: function (context, settings) {
      if(!this.carousel_configured) {

        if($('.campl-slides .campl-slide').length > 1) {
          addCarouselControls();
        }

        // After resizing, we force a page reload to make sure the carousel
        // layout is properly updated.  We add a short debounce to make sure
        // we are not doing this constantly through window resizing though.
        $(window).resize(function() {
          clearTimeout(window.resizeEnded);
          window.resizeEnded = setTimeout(function() {
            resizeCarousel();
            location.reload();
          }, 250);
        });

        // make sure to properly size the carousel on initial page load
        resizeCarousel();
        this.carousel_configured = true;
      }
    },
  };

  function resizeCarousel() {
    carouselContainer = $(".campl-slides").first();
    carouselWidth = carouselContainer.width();
    carouselContainer.find('.campl-slide').css({width:carouselWidth, height: 'auto'});

    if (Modernizr.mq('only screen and (max-width: 767px)')) {
      var height = $(".campl-carousel .image-container").first().height();
      $(".campl-carousel-controls, .campl-carousel-controls a.campl-previous, .campl-carousel-controls a.campl-next").height(height);
    } else {
      $(".campl-carousel-controls, .campl-carousel-controls a.campl-previous, .campl-carousel-controls a.campl-next").height('');
    }

  }

  function addCarouselControls() {
    carouselContainer = $(".campl-carousel").first();

    var carouselControls = document.createElement('ul');
    carouselControls.className = 'campl-unstyled-list clearfix campl-carousel-controls';
    carouselControls.id = 'carousel-controls';

    var previouslistItem = document.createElement('li');
    previouslistItem.className = "campl-previous-li";
    var previouslink = document.createElement('a');
    var previousArrowSpan = document.createElement('span');
    previousArrowSpan.className = "campl-arrow-span";
    previouslink.className = "ir campl-carousel-control-btn campl-previous";
    var previouslinkText = document.createTextNode('previous slide');
    previouslink.setAttribute('href', '#');
    previouslink.appendChild(previousArrowSpan);
    previouslink.appendChild(previouslinkText);
    previouslistItem.appendChild(previouslink);
    carouselControls.appendChild(previouslistItem);

    var pauselistItem = document.createElement('li');
    pauselistItem.className = "campl-pause-li";
    var pauselink = document.createElement('a');
    var pauseArrowSpan = document.createElement('span');
    pauseArrowSpan.className = "campl-arrow-span";
    pauselink.className = "ir campl-carousel-control-btn campl-pause";
    var pauselinkText = document.createTextNode('pause slides');
    pauselink.setAttribute('href', '#');
    pauselink.appendChild(pauseArrowSpan);
    pauselink.appendChild(pauselinkText);
    pauselistItem.appendChild(pauselink);
    carouselControls.appendChild(pauselistItem);

    var nextlistItem = document.createElement('li');
    nextlistItem.className = "campl-next-li";
    var nextlink = document.createElement('a');
    var nextArrowSpan = document.createElement('span');
    nextArrowSpan.className = "campl-arrow-span";
    nextlink.className = "ir campl-carousel-control-btn campl-next";
    var nextlinkText = document.createTextNode('next slide');
    nextlink.setAttribute('href', '#');
    nextlink.appendChild(nextArrowSpan);
    nextlink.appendChild(nextlinkText);
    nextlistItem.appendChild(nextlink);
    carouselControls.appendChild(nextlistItem);
    carouselContainer.append(carouselControls);

    /**
     * On prev/next, we:
     *   - cycle() in the relevant direction
     *   - pause the carousel
     *   - ensure the campl-pause becomes campl-play
     * on the basis that if a user has chosen to manually interact with prev/next
     * it is because they want to view a particular slide rather than have the
     * cycling slideshow.
     *
     * For the pause/play button,
     *   - if paused, move to next and resume
     *   - toggle the slideshow state
     *   - toggle the play/pause class (and thus the icon)
     */
    $('.campl-previous').click(function(event){
      $('#views_slideshow_cycle_teaser_section_front_page_carousel-block_front_page').cycle('prev');
      $('#views_slideshow_cycle_teaser_section_front_page_carousel-block_front_page').cycle('pause');
      $('.campl-pause').toggleClass("campl-pause campl-play");
    });

    $('.campl-next').click(function(event){
      $('#views_slideshow_cycle_teaser_section_front_page_carousel-block_front_page').cycle('next');
      $('#views_slideshow_cycle_teaser_section_front_page_carousel-block_front_page').cycle('pause');
      $('.campl-pause').toggleClass("campl-pause campl-play");
    });

    $('.campl-pause').click(function(event){
      // If we have a .campl-play button we are paused, so we want to immediately move to the next
      // slide if clicked.
      if ($('.campl-play').length > 0) {
        $('#views_slideshow_cycle_teaser_section_front_page_carousel-block_front_page').cycle('next');
      }
      $('#views_slideshow_cycle_teaser_section_front_page_carousel-block_front_page').cycle('toggle');
      $('.campl-play, .campl-pause').toggleClass("campl-pause campl-play");
    });

    carouselContainer.find("ul").css({'z-index':1000});
    carouselContainer.find("li").show();
  }
}) (Drupal, jQuery);
