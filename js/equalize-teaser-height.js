(function($) {

  Drupal.behaviors.teaser_heights = {
    teaser_height_processed: false,
    attach: function (context, settings) {
      if(!this.teaser_height_processed) {
        if (!Modernizr.mq('only screen and (max-width: 767px)')) {
          setTeaserHeight();
          $(window).on('resize', function() {
            setTeaserHeight();
          });
        } else {
          removeTeaserHeight();
        }
        this.content_column_height_processed = true;
      }
    },
  };

  function setTeaserHeight() {
    var maxHeight = 0;

    // Find all elements with the selector
    $('.block .campl-teaser-border').each(function() {
      var elementHeight = $(this).outerHeight();
      if (elementHeight > maxHeight) {
        maxHeight = elementHeight;
      }
    });

    // Set the maximum height on all elements
    $('.block .campl-teaser-border').each(function() {
      $(this).css('height', maxHeight + 'px');
    });
  }

  function removeTeaserHeight() {
    $('.block .campl-teaser-border').each(function() {
      $(this).css('height', '');
    });
  }


})(jQuery);
