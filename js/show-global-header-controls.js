(function(Drupal, $) {

  Drupal.behaviors.show_global_header_controls = {
    attach: function(context, settings) {
      // This element is hidden in css to avoid FOUC, so show it on page load.
      var $globalHeaderControls = $("#global-header-controls");
      if ($globalHeaderControls.is(':hidden')) {
        $globalHeaderControls.show();
      }
    }
  }

}) (Drupal, jQuery);
