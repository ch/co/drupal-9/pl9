(function(Drupal, $) {

  Drupal.behaviors.autostyle_images = {
    attach: function(context, settings) {
      $('img').not('.no-auto-scale img').not('.no-auto-scale').not('.campl-scale-with-grid').each(function() {
        $(this).addClass('campl-scale-with-grid');
        $(this).css("height", "auto");
      });
    }
  }

}) (Drupal, jQuery);
