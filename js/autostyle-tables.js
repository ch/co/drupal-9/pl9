(function(Drupal, $) {

  Drupal.behaviors.autostyle_tables = {
    attach: function(context, settings) {
      $('.region-content table:not(.campl-table):not(.campl-table-custom)', context).addClass('campl-table campl-table-bordered campl-table-striped campl-vertical-stacking-table').attr('border', 0).attr('cellpadding', 0).attr('cellspacing', 0).attr('style', null);
    }
  }

}) (Drupal, jQuery);
