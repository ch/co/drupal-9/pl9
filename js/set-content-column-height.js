(function(Drupal, $) {

  Drupal.behaviors.set_content_column_height = {
    content_column_height_processed: false,
    attach: function (context, settings) {
      if(!this.content_column_height_processed) {
        if (!Modernizr.mq('only screen and (max-width: 767px)')) {
          setContentColumnHeight();
        } else {
          removeContentColumnHeight();
        }
        this.content_column_height_processed = true;
      }
    },
  };


  function setContentColumnHeight() {
    $('.campl-secondary-content, .campl-main-content').removeAttr("style");

    var secondaryContentRecessedHeight = 0;

    if($('.campl-secondary-content').hasClass("campl-recessed-secondary-content")) {
      secondaryContentRecessedHeight = ($('.campl-secondary-content').parent().width() / 100) * 36.6;
    }

		// If we end up with any of these heights being undefined, Math.max() will return NaN
    var camplSecondaryContentHeight = $('.campl-secondary-content').height() ?? 0;
    var camplMainContentHeight = $('.campl-main-content').height() ?? 0;

    var maxColumnHeight = Math.max(camplSecondaryContentHeight - secondaryContentRecessedHeight, camplMainContentHeight);

    $('.campl-secondary-content, .campl-main-content').css({'min-height':maxColumnHeight});
    $('.campl-secondary-content').css({'min-height':maxColumnHeight +50});

    if($('.campl-secondary-content').hasClass("campl-recessed-secondary-content")){
      $('.campl-secondary-content').css({'min-height':maxColumnHeight + secondaryContentRecessedHeight });
    }

    $('.campl-secondary-content').show();

  }

  function removeContentColumnHeight() {
    //had to remove style attribute, as setting height back to auto would not work
    $('.campl-secondary-content, .campl-main-content').removeAttr("style");
    $('.campl-secondary-content, .campl-main-content').show();
  }


}) (Drupal, jQuery);
