(function(Drupal, $, once) {

  Drupal.behaviors.leading_images = {
    attach: function(context, settings) {
      once('pl9_leading_images', '.campl-main-content', context).forEach(function(element) {
        $('.campl-main-content.campl-column6 .field--name-field-image img').each(function() {
          if ($(this).attr('data-src-6')) {
              $(this).attr('src', $(this).attr('data-src-6'));
              $(this).attr('width', 590);
              $(this).attr('height', 'auto');
          }
        });

        $('.campl-main-content.campl-column9 .field--name-field-image img').each(function() {
          if ($(this).attr('data-src-9')) {
              $(this).attr('src', $(this).attr('data-src-9'));
              $(this).attr('width', 885);
              $(this).attr('height', 'auto');
          }
        });

        $('.campl-main-content.campl-column12 .field--name-field-image img').each(function() {
          if ($(this).attr('data-src-12')) {
              $(this).attr('src', $(this).attr('data-src-12'));
              $(this).attr('width', 1177);
              $(this).attr('height', 'auto');
          }
        });
      })
    }
  }

}) (Drupal, jQuery, once);
