<?php

/**
 * @file
 * Settings form for project light theme.
 */

/**
 * Implements hook_form_system_theme_settings_alter().
 */
function pl9_form_system_theme_settings_alter(&$form, $form_state) {
  global $base_url;

  $form['pl9'] = [
    '#type' => 'fieldset',
    '#title' => t('Theme settings'),
    '#weight' => 0,
  ];

  $form['pl9']['colour_scheme'] = [
    '#type' => 'select',
    '#title' => t('Colour scheme'),
    '#options' => [
      1 => 'Blue',
      2 => 'Turquoise',
      3 => 'Purple',
      4 => 'Green',
      5 => 'Orange',
      6 => 'Red',
      7 => 'Grey',
      8 => 'Lime',
      9 => 'Blue-grey',
    ],
    '#default_value' => theme_get_setting('colour_scheme'),
  ];

  $form['pl9']['display_slogan'] = [
    '#type' => 'checkbox',
    '#title' => 'Display site slogan',
    '#default_value' => theme_get_setting('display_slogan'),
  ];

  $form['pl9']['search_box'] = [
    '#type' => 'radios',
    '#title' => t('Search box'),
    '#description' => t('Choose what the search box in the global navigation searches.'),
    '#options' => [
      0 => 'Whole University',
      1 => 'This site (i.e. ' . $base_url . ')',
      2 => 'Search engine filter',
    ],
    '#default_value' => theme_get_setting('search_box'),
  ];
  $form['pl9']['search_box_filter'] = [
    '#type' => 'fieldset',
    '#title' => t('Filter settings'),
    '#description' => "These details must match a filter configured in the University's search engine.",
    '#states' => [
      'visible' => [
        ':input[name="search_box"]' => ['value' => 2],
      ],
    ],
  ];
  $form['pl9']['search_box_filter']['search_box_filter_inst'] = [
    '#type' => 'textfield',
    '#title' => t('Institution filter code'),
    '#default_value' => theme_get_setting('search_box_filter_inst'),
    '#states' => [
      'visible' => [
        ':input[name="search_box"]' => ['value' => 2],
      ],
      'required' => [
        ':input[name="search_box"]' => ['value' => 2],
      ],
    ],
  ];
  $form['pl9']['search_box_filter']['search_box_filter_tag'] = [
    '#type' => 'textfield',
    '#title' => t('Tag'),
    '#default_value' => theme_get_setting('search_box_filter_tag'),
    '#states' => [
      'visible' => [
        ':input[name="search_box"]' => ['value' => 2],
      ],
    ],
  ];
}
